// ***************************
// ********* Includes ********
// ***************************

#include <Arduino.h>
#include <Arduino_GFX_Library.h>
#include <font/FreeMonoBold48pt7b.h>
#include <font/FreeMonoBold8pt7b.h>
// #include <LiquidCrystalFast.h>
#include <LiquidCrystal.h>
#include <Adafruit_NeoPixel.h>
#include <RotaryEncoder.h>
#include <Adafruit_MCP23X17.h>
// #include <Wire.h>
#include <LiquidCrystal.h>

#include "pins.h"

#define NUMBER_OF_NEOPIXELS     24      // Number of pixels in one neopixel ring
#define NUMBER_OF_LEDS          6       // Number of led buttons
#define NUMBER_OF_EXP_BUTTONS   6

#define BUTTON_DEBOUNCE_TIME    50      // ms

#define JOYSTICK_DEADZONE       0.25
#define JOYSTICK_NUM_AXES       6
#define JOYSTICK_UPDATE_INTERVAL    300         // ms

// Used to calculate the positions of the rotaryswitch
// The rotaryswitch is connected via a single analog input
#define ROTARY_SWITCH_CENTER_RANGE      85

#define DBG_SERIAL      Serial
#define RPI_SERIAL      Serial8 //Serial8

// First byte of every message
// used to determine the data that is sent in the message
#define READ_ENCODER_A           0
#define READ_ENCODER_B           1
#define READ_BTN_A_X             2
#define READ_BTN_A_Y             3
#define READ_BTN_A_Z             4
#define READ_BTN_B_X             5
#define READ_BTN_B_Y             6
#define READ_BTN_B_Z             7
#define READ_JOYSTICK_A_BTN      8
#define READ_JOYSTICK_B_BTN      9
#define READ_JOYSTICK_A_X       10
#define READ_JOYSTICK_A_Y       11
#define READ_JOYSTICK_A_Z       12
#define READ_JOYSTICK_B_X       13
#define READ_JOYSTICK_B_Y       14
#define READ_JOYSTICK_B_Z       15
#define READ_ROTARY_SWITCH      16

#define WRITE_CHARLCD            0
#define WRITE_ROUNDLCD           1
#define WRITE_LEDS               2
#define WRITE_LED_A_X            3
#define WRITE_LED_A_Y            4
#define WRITE_LED_A_Z            5
#define WRITE_LED_B_X            6
#define WRITE_LED_B_Y            7
#define WRITE_LED_B_Z            8
#define WRITE_NEOPIXEL_A         9
#define WRITE_NEOPIXEL_B        10


// ***************************
// ********* Structs *********
// ***************************

struct RotarySwitch {
	uint8_t value;
	uint8_t read_index;
	uint8_t lastValue;
	boolean dirty;
};
RotarySwitch rotarySwitch;

struct led {
	boolean value;
	uint8_t write_index;
	uint8_t pin;
};
led leds[NUMBER_OF_LEDS];

struct button {
	boolean value;
	uint8_t read_index;
	uint8_t pin;
	boolean lastValue;
	boolean dirty;
};
button exp_buttons[NUMBER_OF_EXP_BUTTONS];
button joystick_buttons[2];

struct axis {
	uint8_t		pin;
	uint8_t		read_index;
	uint16_t	min;
	uint16_t	max;
	uint16_t	deadzone;
	uint16_t	center;
	float		value;
	uint16_t	raw;
	boolean		dirty;
	boolean		centerDirty;
};
axis axes[JOYSTICK_NUM_AXES];

// ***************************
// ********* Objects *********
// ***************************

Adafruit_MCP23X17 port_expander;

RotaryEncoder encoder_a(PIN_ENCODER_A_A, PIN_ENCODER_A_B, RotaryEncoder::LatchMode::TWO03);
RotaryEncoder encoder_b(PIN_ENCODER_B_A, PIN_ENCODER_B_B, RotaryEncoder::LatchMode::FOUR0);

Adafruit_NeoPixel neopixel_a(NUMBER_OF_NEOPIXELS, PIN_NEOPIXEL_A, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel neopixel_b(NUMBER_OF_NEOPIXELS, PIN_NEOPIXEL_B, NEO_GRB + NEO_KHZ800);

LiquidCrystal char_lcd_top(PIN_CHARLCD_RS, PIN_CHARLCD_RW, PIN_CHARLCD_EN1, PIN_CHARLCD_D4, PIN_CHARLCD_D5, PIN_CHARLCD_D6, PIN_CHARLCD_D7);
LiquidCrystal char_lcd_bot(PIN_CHARLCD_RS, PIN_CHARLCD_RW, PIN_CHARLCD_EN2, PIN_CHARLCD_D4, PIN_CHARLCD_D5, PIN_CHARLCD_D6, PIN_CHARLCD_D7);

Arduino_DataBus *bus = new Arduino_HWSPI(PIN_ROUNDLCD_DC /* DC */, PIN_ROUNDLCD_CS /* CS */);
Arduino_GFX *output_display = new Arduino_GC9A01(bus, PIN_ROUNDLCD_RST /* RST */, 0 /* rotation */, true /* IPS */);
Arduino_GFX *round_lcd = new Arduino_Canvas(240 /* width */, 240 /* height */, output_display);

// ***************************
// ******** Variables ********
// ***************************

int new_encoder_pos_a = 0;
int new_encoder_pos_b = 0;

byte round_lcd_x = 36;
byte round_lcd_y = 148;

unsigned long round_lcd_timestamp = 0;
bool round_lcd_dirty = false;
size_t receivedBytes;
unsigned long joystick_timestamp = 0;
unsigned long button_timestamp = 0;

uint8_t write_led_value = 0;

// ***************************
// ********* Arrays **********
// ***************************

uint8_t backslash[8] = { 0x00, 0x10, 0x08, 0x04, 0x02, 0x01, 0x00, 0x00 };
char pixelBuffer[NUMBER_OF_NEOPIXELS * 3];
char stringBuffer[3];
char serialBuffer[180];

uint8_t red_pixel[] = { 255, 0, 0 };
uint8_t clear_pixel[] = { 0, 0, 0 };
uint8_t led_sequence[][6] = {
	{ 1, 0, 0, 0, 0, 0 },
	{ 0, 1, 0, 0, 0, 0 },
	{ 0, 0, 1, 0, 0, 0 },
	{ 0, 0, 0, 0, 0, 1 },
	{ 0, 0, 0, 0, 1, 0 },
	{ 0, 0, 0, 1, 0, 0 }
};

char banner[][40] = {
/*0         0         0         0
 * 0123456789012345678901234567890123456789
 */
	{ "     ___  ___   ___  __________  ___   "	},
	{ "    / _ \\/ _ | / _ \\/_  __/ __ \\/ _ \\  " },
	{ "   / , _/ __ |/ ___/ / / / /_/ / , _/  "	},
	{ "  /_/|_/_/ |_/_/    /_/  \\____/_/|_|   "	}
// {"BASE |000.00|000.00|000.00|Idle |  12000"},
// {"HEAD |000.00|000.00|000.00|     |       "},
// {"PLATE|000.00|000.00|000.00|Idle |   6000"},
// {"TIME | 00:26.50/00:30.00  |Idle |211/249"}
};

char characterDisplayBuffer[4][40];


// ***************************
// ******* Prototypes ********
// ***************************


void draw_number_and_arc(int number, int max_number);
void boot_animation();
void setupAnalogAxes();
void setupLeds();
void setupButtons();
void setupNeopixels();
void setupRoundLCD();
void clear_leds();
void clear_pixels();
void calcJoystic(axis& ax);
float calcAxis(axis& a, uint16_t value);
void updateButton(button &btn, boolean val);
int readRotarySwitch();
void updateRotarySwitch();
void printCharacterDisplay(char buffer[][40]);
void printCharacterDisplayAtPosition(char character, size_t row, size_t col);

// ***************************
// ********** Setup **********
// ***************************

void setup() {
	DBG_SERIAL.begin(115200);
	RPI_SERIAL.begin(115200);

	DBG_SERIAL.println("");
	DBG_SERIAL.println("");
	DBG_SERIAL.println("");
	DBG_SERIAL.println("****************************************");
	DBG_SERIAL.println(banner[0]);
	DBG_SERIAL.println(banner[1]);
	DBG_SERIAL.println(banner[2]);
	DBG_SERIAL.println(banner[3]);
	DBG_SERIAL.println("");
	DBG_SERIAL.println("****************************************");

	DBG_SERIAL.print("Setup encoders...");
	pinMode(PIN_ENCODER_A_A, INPUT_PULLUP);
	pinMode(PIN_ENCODER_A_B, INPUT_PULLUP);
	pinMode(PIN_ENCODER_B_A, INPUT_PULLUP);
	pinMode(PIN_ENCODER_B_B, INPUT_PULLUP);
	DBG_SERIAL.println("done");

	DBG_SERIAL.print("Setup character lcd...");
	char_lcd_top.begin(40, 2);
	char_lcd_bot.begin(40, 2);
	char_lcd_top.createChar(1, backslash);
	char_lcd_bot.createChar(1, backslash);
	char_lcd_top.setCursor(0, 0);
	char_lcd_bot.setCursor(0, 0);
	char_lcd_top.clear();
	char_lcd_bot.clear();
	DBG_SERIAL.println("done");

	DBG_SERIAL.print("Setup port expander...");
	if (!port_expander.begin_I2C(MCP23XXX_ADDR, &Wire1)) {
		while (1) {
			DBG_SERIAL.println("I2C Epander ERROR");
			delay(1000);
		}
	}
	DBG_SERIAL.println("done");

	DBG_SERIAL.print("Setup neopixels...");
	setupNeopixels();
	DBG_SERIAL.println("done");

	DBG_SERIAL.print("Setup round lcd...");
	setupRoundLCD();
	DBG_SERIAL.println("done");

	DBG_SERIAL.print("Setup leds...");
	setupLeds();
	DBG_SERIAL.println("done");

	DBG_SERIAL.print("Setup buttons...");
	setupButtons();
	DBG_SERIAL.println("done");

	DBG_SERIAL.print("Setup analog axes...");
	setupAnalogAxes();
	DBG_SERIAL.println("done");

	DBG_SERIAL.print("Playing boot animation...");
	boot_animation();
	DBG_SERIAL.println("done");

	DBG_SERIAL.println("Setup done. Starting");
	DBG_SERIAL.println("****************************************");
}

// ***************************
// *********** Loop **********
// ***************************

void loop() {
	// Process commands from Raspberry Pi
	while (RPI_SERIAL.available()) {
		int id = Serial.read();
		switch (id) {
		case WRITE_CHARLCD:
			DBG_SERIAL.println("WRITE_CHARLCD");
			// Todo: Check length of received content.
			// Might be too short and trigger the timeout => wasting time with waiting
			receivedBytes = Serial.readBytes(serialBuffer, 4 * 40 + 1);
			for (size_t i = 0; i < 4; i++) {
				for (size_t j = 0; j < 40; j++) {
					characterDisplayBuffer[i][j] = serialBuffer[i * 40 + j];
				}
			}
			printCharacterDisplay(characterDisplayBuffer);

			break;
		case WRITE_ROUNDLCD:
			DBG_SERIAL.println("WRITE_ROUNDLCD");
			receivedBytes = Serial.readBytes(serialBuffer, 4);
			DBG_SERIAL.print("receivedBytes: ");
			DBG_SERIAL.println(receivedBytes);
			DBG_SERIAL.print("number: ");
			DBG_SERIAL.println(serialBuffer[1] | serialBuffer[0] << 8);
			DBG_SERIAL.print("max: ");
			DBG_SERIAL.println(serialBuffer[3] | serialBuffer[2] << 8);

			draw_number_and_arc(
				serialBuffer[1] | serialBuffer[0] << 8,
					serialBuffer[3] | serialBuffer[2] << 8
				);

			break;
		case WRITE_LEDS:
			DBG_SERIAL.println("WRITE_LEDS");
			//  0x02 0x3f => 0b00111111	Turn on all leds
			write_led_value = Serial.read();
			for (size_t i = 0; i < NUMBER_OF_LEDS; i++) {
				port_expander.digitalWrite(leds[i].pin, (write_led_value >> i) & 0x01);
			}
			break;
		case WRITE_LED_A_X:
			DBG_SERIAL.println("WRITE_LED_A_X");
			port_expander.digitalWrite(leds[0].pin, Serial.read());
			break;
		case WRITE_LED_A_Y:
			DBG_SERIAL.println("WRITE_LED_A_Y");
			port_expander.digitalWrite(leds[1].pin, Serial.read());
			break;
		case WRITE_LED_A_Z:
			DBG_SERIAL.println("WRITE_LED_A_Z");
			port_expander.digitalWrite(leds[2].pin, Serial.read());
			break;
		case WRITE_LED_B_X:
			DBG_SERIAL.println("WRITE_LED_B_X");
			port_expander.digitalWrite(leds[3].pin, Serial.read());
			break;
		case WRITE_LED_B_Y:
			DBG_SERIAL.println("WRITE_LED_B_Y");
			port_expander.digitalWrite(leds[4].pin, Serial.read());
			break;
		case WRITE_LED_B_Z:
			DBG_SERIAL.println("WRITE_LED_B_Z");
			port_expander.digitalWrite(leds[5].pin, Serial.read());
			break;
		case WRITE_NEOPIXEL_A:
			DBG_SERIAL.println("WRITE_NEOPIXEL_A");
			neopixel_a.clear();
			receivedBytes = Serial.readBytes(pixelBuffer, NUMBER_OF_NEOPIXELS * 3);

			for (size_t i = 0; i < receivedBytes; i += 3) {
				neopixel_a.setPixelColor(
					i / 3,
					pixelBuffer[i],
					pixelBuffer[i + 1],
					pixelBuffer[i + 2]
					);
			}
			neopixel_a.show();

			break;
		case WRITE_NEOPIXEL_B:
			DBG_SERIAL.println("WRITE_NEOPIXEL_B");
			neopixel_b.clear();
			receivedBytes = Serial.readBytes(pixelBuffer, NUMBER_OF_NEOPIXELS * 3);

			for (size_t i = 0; i < receivedBytes; i += 3) {
				neopixel_b.setPixelColor(
					i / 3,
					pixelBuffer[i],
					pixelBuffer[i + 1],
					pixelBuffer[i + 2]
					);
			}
			neopixel_b.show();
			break;
		}
	}

	// Process encoders and send data if available
	encoder_a.tick();
	new_encoder_pos_a = encoder_a.getPosition();
	if (new_encoder_pos_a != 0) {
		DBG_SERIAL.print("Encoder A: ");
		DBG_SERIAL.println(encoder_a.getPosition());

		RPI_SERIAL.write(READ_ENCODER_A);
		RPI_SERIAL.write(encoder_a.getPosition());

		encoder_a.setPosition(0);
	}

	encoder_b.tick();
	new_encoder_pos_b = encoder_b.getPosition();
	if (new_encoder_pos_b != 0) {
		DBG_SERIAL.print("Encoder B: ");
		DBG_SERIAL.println(encoder_b.getPosition());

		RPI_SERIAL.write(READ_ENCODER_B);
		RPI_SERIAL.write(encoder_b.getPosition());

		encoder_b.setPosition(0);
	}

	// Process joysticks and send data if available
	if (millis() - joystick_timestamp > JOYSTICK_UPDATE_INTERVAL) {
		for (size_t i = 0; i < JOYSTICK_NUM_AXES; i++) {
			calcJoystic(axes[i]);
			if (axes[i].dirty) {
				DBG_SERIAL.print("Joystick Axis: ");
				DBG_SERIAL.print(i);
				DBG_SERIAL.print(" : ");
				DBG_SERIAL.println(axes[i].value);

				RPI_SERIAL.write(axes[i].read_index);
				char *c_Data = (char *)&axes[i].value;
				for (char c_Index = 0; c_Index < sizeof(float); c_Index++) {
					RPI_SERIAL.write(c_Data[c_Index]);
				}
				axes[i].dirty = false;
			}
		}
	}

	// Process buttons and send data if available
	if (millis() - button_timestamp > BUTTON_DEBOUNCE_TIME) {
		// buttons at the port expander
		for (size_t i = 0; i < NUMBER_OF_EXP_BUTTONS; i++) {
			updateButton(exp_buttons[i], port_expander.digitalRead(exp_buttons[i].pin));
			if (exp_buttons[i].dirty) {
				DBG_SERIAL.print("Button ");
				DBG_SERIAL.print(i);
				DBG_SERIAL.print(": ");
				DBG_SERIAL.println(exp_buttons[i].value);

				RPI_SERIAL.write(exp_buttons[i].read_index);
				RPI_SERIAL.write(exp_buttons[i].value);

				exp_buttons[i].dirty = false;
			}
		}
		// joystick buttons
		for (size_t i = 0; i < 2; i++) {
			updateButton(joystick_buttons[i], digitalRead(joystick_buttons[i].pin));
			if (joystick_buttons[i].dirty) {
				DBG_SERIAL.print("Joystick Button ");
				DBG_SERIAL.print(i);
				DBG_SERIAL.print(": ");
				DBG_SERIAL.println(joystick_buttons[i].value);

				RPI_SERIAL.write(joystick_buttons[i].read_index);
				RPI_SERIAL.write(joystick_buttons[i].value);

				joystick_buttons[i].dirty = false;
			}
		}
		// rotary switch
		updateRotarySwitch();
		if (rotarySwitch.dirty) {
			DBG_SERIAL.print("Rotary switch position: ");
			DBG_SERIAL.println(rotarySwitch.value);

			RPI_SERIAL.write(READ_ROTARY_SWITCH);
			RPI_SERIAL.write(rotarySwitch.value);

			rotarySwitch.dirty = false;
		}
	}
}



// ***************************
// ******** Functions ********
// ***************************

// pint a single character at a given position
void printCharacterDisplayAtPosition(char character, size_t row, size_t col) {
	if (row < 2) {
		char_lcd_top.setCursor(col, row);
		char_lcd_top.print(character);
	} else {
		char_lcd_bot.setCursor(col, row - 2);
		char_lcd_bot.print(character);
	}
}

// print the entire display
void printCharacterDisplay(char buffer[][40]) {
	char_lcd_top.setCursor(0, 0);
	for (size_t j = 0; j < 2; j++) {
		for (size_t i = 0; i < 40; i++) {
			// find backslash
			if (buffer[j][i] == 92) {
				char_lcd_top.print(char(1)); // replace with custom backslash char
			} else {
				char_lcd_top.print(buffer[j][i]);
			}
		}
	}
	char_lcd_bot.setCursor(0, 0);
	for (size_t j = 2; j < 4; j++) {
		for (size_t i = 0; i < 40; i++) {
			// find backslash
			if (buffer[j][i] == 92) {
				char_lcd_bot.print(char(1)); // replace with custom backslash char
			} else {
				char_lcd_bot.print(buffer[j][i]);
			}
		}
	}
}

// update the rotary switch object
void updateRotarySwitch() {
	int val = readRotarySwitch();
	if (rotarySwitch.value != val) {                // value has changed
		if (rotarySwitch.lastValue == val) {    // debounce
			rotarySwitch.value = val;
			rotarySwitch.dirty = true;
		}
		rotarySwitch.lastValue = val;
	}
}

// read the value from the rotary switch
// converts the analog value from the pin to a range of integers
int readRotarySwitch() {
	// 1022		BZ
	// 852		BY
	// 685		BX
	// 504		AZ
	// 338		AY
	// 168		AX
	int val = analogRead(PIN_ROTARY_SWITCH);

	if (val > 1022 - ROTARY_SWITCH_CENTER_RANGE) {
		return 5;
	} else if (val < 852 + ROTARY_SWITCH_CENTER_RANGE && val > 852 - ROTARY_SWITCH_CENTER_RANGE) {
		return 4;
	} else if (val < 685 + ROTARY_SWITCH_CENTER_RANGE && val > 685 - ROTARY_SWITCH_CENTER_RANGE) {
		return 3;
	} else if (val < 504 + ROTARY_SWITCH_CENTER_RANGE && val > 504 - ROTARY_SWITCH_CENTER_RANGE) {
		return 2;
	} else if (val < 338 + ROTARY_SWITCH_CENTER_RANGE && val > 338 - ROTARY_SWITCH_CENTER_RANGE) {
		return 1;
	} else if (val < 168 + ROTARY_SWITCH_CENTER_RANGE && val > 168 - ROTARY_SWITCH_CENTER_RANGE) {
		return 0;
	}
	return -1;
}

// update the passed button struct
// handles debounce
void updateButton(button &btn, boolean val) {
	if (btn.value != val) {                 // value has changed
		if (btn.lastValue == val) {     // debounce
			btn.value = val;
			btn.dirty = true;
		}
		btn.lastValue = val;
	}
}

// draw a number in a arc on the round display
// the arc represents the percentage of  the number in relation to the max_number
void draw_number_and_arc(int number, int max_number) {
	round_lcd->fillScreen(BLACK);
	float deg = 130 + (280 * ((float)number / (float)max_number));
	sprintf(stringBuffer, "%03d", number);

	round_lcd->setCursor(round_lcd_x, round_lcd_y);
	round_lcd->println(stringBuffer);
	round_lcd->drawRoundRect(round_lcd_x, 84, 168, 72, 10, GREEN);
	round_lcd->fillArc(120, 120, 115, 105, deg, 50, BLACK);
	round_lcd->fillArc(120, 120, 115, 105, 130, deg, GREEN);
	round_lcd->drawArc(120, 120, 118, 102, 129, 51, RED);   round_lcd_dirty = true;

	round_lcd->flush();
}

// calculate float value on single axis without deadzones
float calcAxis(axis& a, uint16_t value) {
	if (value > a.center) {         // positive
		return (float)(value - a.center) / (a.max - a.center);
	} else if (value < a.center) {  // negative
		return (float)(a.center - value) / (a.center - a.min) * -1;
	} else {
		return 0.0;
	}
}

// calculates the value of the passed axis based on the calibration values and the deadzones
void calcJoystic(axis& ax) {
	ax.raw = analogRead(ax.pin);
	float oldValueX = ax.value;
	float valueX = calcAxis(ax, ax.raw);

	if (abs(valueX) > JOYSTICK_DEADZONE) {
		if (valueX > 0) {
			ax.value = (float)((ax.raw - ax.center) - ax.deadzone) / ((ax.max - ax.center) - ax.deadzone);
		} else if (valueX < 0) {
			ax.value = (1.0 - ((float)(ax.raw - ax.min) / ((ax.center - ax.min) - ax.deadzone))) * -1;
		} else {
			ax.value = 0.0;
		}
		if (ax.value > 1) {
			ax.value = 1;
		}
		if (ax.value < -1) {
			ax.value = -1;
		}
	} else {
		ax.value = 0.0;
	}

	// rounding
	ax.value = (int)(ax.value * 100.0) / -100.0;

	if (oldValueX != ax.value) {
		ax.dirty = true;
	}

	if ((oldValueX != 0.0) && (ax.value == 0.0)) {
		ax.centerDirty = true;
	}
}

// Play the boot animation
// tests all output hardware
void boot_animation(){
	round_lcd->fillScreen(BLACK);
	round_lcd->flush();
	clear_leds();
	clear_pixels();

	for (size_t k = 0; k < 4; k++) {
		for (size_t l = 0; l < 41; l++) {
			characterDisplayBuffer[k][l] = random(166, 222);
		}
	}

	for (size_t i = 0; i < 12; i++) {
		round_lcd->fillScreen(BLACK);
		round_lcd->fillRect(120 - i * 10, 118, i * 20, 4, GREEN);
		round_lcd->flush();
		delay(30);
	}

	size_t pixel_pos = 0;
	size_t led_pos = 0;
	size_t frames = 60;
	for (size_t i = 0; i < frames; i++) {
		round_lcd->fillScreen(BLACK);
		round_lcd->fillArc(120, 120, 10 + i * 2, i * 2, i * 18, 60 + i * 18, GREEN);
		round_lcd->fillArc(120, 120, 10 + i * 2, i * 2, 120 + i * 18, 180 + i * 18, GREEN);
		round_lcd->fillArc(120, 120, 10 + i * 2, i * 2, 240 + i * 18, 300 + i * 18, GREEN);

		for (size_t j = 0; j < NUMBER_OF_NEOPIXELS; j++) {
			if (j == pixel_pos || j == pixel_pos + NUMBER_OF_NEOPIXELS / 3 || j == pixel_pos + (NUMBER_OF_NEOPIXELS / 3) * 2 || i >= frames - NUMBER_OF_NEOPIXELS + j) {
				neopixel_a.setPixelColor(j, red_pixel[0], red_pixel[1], red_pixel[2]);
				neopixel_b.setPixelColor(j, red_pixel[0], red_pixel[1], red_pixel[2]);
			} else {
				neopixel_a.setPixelColor(j, clear_pixel[0], clear_pixel[1], clear_pixel[2]);
				neopixel_b.setPixelColor(j, clear_pixel[0], clear_pixel[1], clear_pixel[2]);
			}
		}
		neopixel_a.show();
		neopixel_b.show();
		pixel_pos++;
		if (pixel_pos >= NUMBER_OF_NEOPIXELS / 3) {
			pixel_pos = 0;
		}

		if (i >= frames - 18) {
			port_expander.digitalWrite(leds[(i - (frames - 18))].pin / 3, HIGH);
		} else {
			for (size_t k = 0; k < NUMBER_OF_LEDS; k++) {
				port_expander.digitalWrite(leds[k].pin, led_sequence[led_pos / 3][k]);
			}

			led_pos++;

			if (led_pos >= 18) {
				led_pos = 0;
			}
		}

		for (size_t j = 0; j < i / 4; j++) {
			printCharacterDisplayAtPosition(random(48, 58), random(0, 4), random(0, 40));
		}
		round_lcd->flush();
	}

	for (size_t k = 0; k < NUMBER_OF_LEDS; k++) {
		// Turn on all leds
		port_expander.digitalWrite(leds[k].pin, 1);
	}

	round_lcd->setFont(&FreeMonoBold8pt7b);

	for (size_t i = 1; i < 7; i++) {
		for (size_t k = 0; k < 4; k++) {
			for (size_t l = 0; l < 40; l++) {
				characterDisplayBuffer[k][l] = random(48, 58);
			}
		}
		printCharacterDisplay(characterDisplayBuffer);

		round_lcd->fillScreen(BLACK);
		if (i == 6) {
			round_lcd->setFont(&FreeMonoBold48pt7b);
			round_lcd->setTextSize(1);
		} else {
			round_lcd->setTextSize(i);
		}
		sprintf(stringBuffer, "R6S");
		round_lcd->setCursor(round_lcd_x + 2, round_lcd_y);
		round_lcd->println(stringBuffer);
		round_lcd->drawRoundRect(round_lcd_x, 84, 178 * (((float)i) / 6), 78 * (((float)i) / 6), 10 * (((float)i) / 6), GREEN);
		round_lcd->flush();
		// delay(10);
	}
	printCharacterDisplay(banner);
	delay(2000);

	for (size_t i = 0; i < NUMBER_OF_NEOPIXELS; i++) {
		for (size_t j = 0; j < NUMBER_OF_NEOPIXELS; j++) {
			if (j > i) {
				neopixel_a.setPixelColor(j, red_pixel[0], red_pixel[1], red_pixel[2]);
				neopixel_b.setPixelColor(j, red_pixel[0], red_pixel[1], red_pixel[2]);
			} else {
				neopixel_a.setPixelColor(j, clear_pixel[0], clear_pixel[1], clear_pixel[2]);
				neopixel_b.setPixelColor(j, clear_pixel[0], clear_pixel[1], clear_pixel[2]);
			}
		}
		neopixel_a.show();
		neopixel_b.show();
		delay(20);
	}

	clear_leds();
	clear_pixels();
	char_lcd_top.clear();
	char_lcd_bot.clear();

	round_lcd->setFont(&FreeMonoBold48pt7b);
	round_lcd->setTextSize(1);
	round_lcd->fillScreen(BLACK);
	round_lcd->flush();
}

// turn all button leds off
void clear_leds(){
	for (size_t i = 0; i < NUMBER_OF_LEDS; i++) {
		port_expander.digitalWrite(leds[i].pin, LOW);
	}
}

// turn all neopixels off
void clear_pixels(){
	for (size_t i = 0; i < NUMBER_OF_NEOPIXELS; i++) {
		neopixel_a.setPixelColor(i, clear_pixel[0], clear_pixel[1], clear_pixel[2]);
		neopixel_b.setPixelColor(i, clear_pixel[0], clear_pixel[1], clear_pixel[2]);
	}
	neopixel_a.show();
	neopixel_b.show();
}

// ***************************
// ***** Setup functions *****
// ***************************

void setupRoundLCD(){
	pinMode(PIN_ROUNDLCD_BL, OUTPUT);
	digitalWrite(PIN_ROUNDLCD_BL, HIGH);
	round_lcd->begin();
	round_lcd->fillScreen(BLACK);
	round_lcd->setCursor(round_lcd_x, round_lcd_y);
	round_lcd->setTextColor(YELLOW, BLACK);
	round_lcd->setFont(&FreeMonoBold48pt7b);
	round_lcd->setTextSize(1);
}

void setupNeopixels(){
	pinMode(PIN_NEOPIXEL_A, OUTPUT);
	pinMode(PIN_NEOPIXEL_B, OUTPUT);
	neopixel_a.begin();
	neopixel_b.begin();
	neopixel_a.clear();
	neopixel_b.clear();
	neopixel_a.show();
	neopixel_b.show();
}


void setupButtons() {
	exp_buttons[0].pin = PIN_EXP_BTN_A_X;
	exp_buttons[0].read_index = READ_BTN_A_X;
	exp_buttons[1].pin = PIN_EXP_BTN_A_Y;
	exp_buttons[1].read_index = READ_BTN_A_Y;
	exp_buttons[2].pin = PIN_EXP_BTN_A_Z;
	exp_buttons[2].read_index = READ_BTN_A_Z;
	exp_buttons[3].pin = PIN_EXP_BTN_B_X;
	exp_buttons[3].read_index = READ_BTN_B_X;
	exp_buttons[4].pin = PIN_EXP_BTN_B_Y;
	exp_buttons[4].read_index = READ_BTN_B_Y;
	exp_buttons[5].pin = PIN_EXP_BTN_B_Z;
	exp_buttons[5].read_index = READ_BTN_B_Z;

	joystick_buttons[0].pin = PIN_JOYSTICK_A_BTN;
	joystick_buttons[0].read_index = READ_JOYSTICK_A_BTN;
	pinMode(joystick_buttons[0].pin, INPUT_PULLUP);

	joystick_buttons[1].pin = PIN_JOYSTICK_B_BTN;
	joystick_buttons[1].read_index = READ_JOYSTICK_B_BTN;
	pinMode(joystick_buttons[1].pin, INPUT_PULLUP);

	for (size_t i = 0; i < NUMBER_OF_EXP_BUTTONS; i++) {
		port_expander.pinMode(exp_buttons[i].pin, INPUT_PULLUP);
	}
}

void setupLeds(){
	leds[0].pin = PIN_EXP_LED_A_X;
	leds[0].write_index = WRITE_LED_A_X;
	leds[1].pin = PIN_EXP_LED_A_Y;
	leds[1].write_index = WRITE_LED_A_Y;
	leds[2].pin = PIN_EXP_LED_A_Z;
	leds[2].write_index = WRITE_LED_A_Z;
	leds[3].pin = PIN_EXP_LED_B_X;
	leds[3].write_index = WRITE_LED_B_X;
	leds[4].pin = PIN_EXP_LED_B_Y;
	leds[4].write_index = WRITE_LED_B_Y;
	leds[5].pin = PIN_EXP_LED_B_Z;
	leds[5].write_index = WRITE_LED_B_Z;

	for (size_t i = 0; i < NUMBER_OF_LEDS; i++) {
		port_expander.pinMode(leds[i].pin, OUTPUT);
		port_expander.digitalWrite(leds[i].pin, LOW);
	}
}

void setupAnalogAxes() {
	axes[0].max = 690;
	axes[0].min = 342;
	axes[0].pin = PIN_JOYSTICK_A_X;
	axes[0].read_index = READ_JOYSTICK_A_X;

	axes[1].max = 675;
	axes[1].min = 331;
	axes[1].pin = PIN_JOYSTICK_A_Y;
	axes[1].read_index = READ_JOYSTICK_A_Y;

	axes[2].max = 729;
	axes[2].min = 281;
	axes[2].pin = PIN_JOYSTICK_A_Z;
	axes[2].read_index = READ_JOYSTICK_A_Z;

	axes[3].max = 690;
	axes[3].min = 342;
	axes[3].pin = PIN_JOYSTICK_B_X;
	axes[3].read_index = READ_JOYSTICK_B_X;

	axes[4].max = 675;
	axes[4].min = 331;
	axes[4].pin = PIN_JOYSTICK_B_Y;
	axes[4].read_index = READ_JOYSTICK_B_Y;

	axes[5].max = 729;
	axes[5].min = 281;
	axes[5].pin = PIN_JOYSTICK_B_Z;
	axes[5].read_index = READ_JOYSTICK_B_Z;

	for (size_t i = 0; i < JOYSTICK_NUM_AXES; i++) {
		axes[i].value = 0.0;
		axes[i].center = ((axes[i].max - axes[i].min) / 2) + axes[i].min;
		axes[i].deadzone = (axes[i].max - axes[i].center) * JOYSTICK_DEADZONE;
		axes[i].raw = 0;
		axes[i].centerDirty = false;
		axes[i].dirty = false;
	}
}
