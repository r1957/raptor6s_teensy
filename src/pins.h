#ifndef PINS_H
#define PINS_H

#define PIN_CHARLCD_RS          0
#define PIN_CHARLCD_RW          1
#define PIN_CHARLCD_EN1         2
#define PIN_CHARLCD_EN2         3
#define PIN_CHARLCD_D4          4
#define PIN_CHARLCD_D5          5
#define PIN_CHARLCD_D6          6
#define PIN_CHARLCD_D7          7
#define PIN_ROUNDLCD_BL         8
#define PIN_ROUNDLCD_RST        9
#define PIN_ROUNDLCD_CS         10
#define PIN_ROUNDLCD_DIN        11
// MISO DO NOT USE 12
#define PIN_ROUNDLCD_CLK        13
#define PIN_ROUNDLCD_DC         14
#define PIN_EXP_INTA            15
#define PIN_EXP_SCK                             16
#define PIN_EXP_SDA                             17
#define PIN_JOYSTICK_A_BTN      18
#define PIN_JOYSTICK_B_BTN      19
#define PIN_JOYSTICK_A_X        20
#define PIN_JOYSTICK_A_Y        21
#define PIN_JOYSTICK_A_Z        22
#define PIN_JOYSTICK_B_X        23
#define PIN_JOYSTICK_B_Y        24
#define PIN_JOYSTICK_B_Z        25
#define PIN_ROTARY_SWITCH       26
#define PIN_NEOPIXEL_A          27
#define PIN_NEOPIXEL_B          28
#define PIN_ENCODER_A_A         29
#define PIN_ENCODER_A_B         30
#define PIN_ENCODER_B_A         31
#define PIN_ENCODER_B_B         32
// 33
#define PIN_RPI_SERIAL_TX       34
#define PIN_RPI_SERIAL_RX       35
// 36
// 37
// 38
// 39
// 40
// 41
#define PIN_MICROSD_DAT1        42
#define PIN_MICROSD_DAT0        43
#define PIN_MICROSD_CLK         44
#define PIN_MICROSD_CMD         45
#define PIN_MICROSD_DAT3        46
#define PIN_MICROSD_DAT2        47

#define PIN_EXP_BTN_B_Z                 0
#define PIN_EXP_LED_B_Z                 1
#define PIN_EXP_BTN_B_Y                 2
#define PIN_EXP_LED_B_Y                 3
#define PIN_EXP_BTN_B_X                 4
#define PIN_EXP_LED_B_X                 5
#define PIN_EXP_LED_A_X                 10
#define PIN_EXP_BTN_A_X                 11
#define PIN_EXP_LED_A_Y                 12
#define PIN_EXP_BTN_A_Y                 13
#define PIN_EXP_LED_A_Z                 14
#define PIN_EXP_BTN_A_Z                 15

#endif
